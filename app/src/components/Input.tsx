import { styled } from "@mui/material/styles";
import { FC } from "react";

interface InputProps {
    value: string;
    onChange: (value: string) => void;
}

const Input: FC<InputProps> = ({
    value,
    onChange,
}: InputProps): JSX.Element => {
    const handleChange = (e: React.SyntheticEvent) => {
        let target = e.target as HTMLInputElement;
        onChange(target.value);
    };

    return <input value={value} type="number" onChange={handleChange} />;
};
export default styled(Input)({});
