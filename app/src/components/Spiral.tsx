import { styled } from "@mui/material/styles";
import { observer } from "mobx-react";
import { FC } from "react";
import * as R from "ramda";
import spiral from "../states/spiral";
import SpiralBox from "./SpiralBox";

interface SpiralProps {}

const Spiral: FC<SpiralProps> = ({}): JSX.Element => {
    const width: number = Math.min(70, spiral.size * 15);
    return (
        <div
            style={{
                position: "absolute",
                top: `50%`,
                left: `50%`,
                transform: spiral.size > 1 ? "translate3d(-50%, -50%, 0)" : "",
                width: `${width}vh`,
                height: `${width}%`,
                maxWidth: "90%",
                maxHeight: "calc(100vw - 20px)",
                // "@media (min-width: 420px)": {
                //     height: `${width}vh`,
                // },
            }}
        >
            {R.map(
                (chunk) => (
                    <SpiralBox key={chunk.index} {...chunk} />
                ),
                spiral.getChunks()
            )}
        </div>
    );
};

export default styled(observer(Spiral))({});
