import { styled } from "@mui/material/styles";
import { observer } from "mobx-react";
import spiral from "../states/spiral";
import Button from "./Button";
import Input from "./Input";

const Config = () => {
    const handleChange = (value: string) => spiral.setSize(value);
    const handleDecrease = () => spiral.decreaseSize();
    const handleIncrease = () => spiral.increaseSize();
    return (
        <div
            style={{
                padding: 5,
                position: "absolute",
                top: `5px`,
                left: `50%`,
                transform: `translate3d(-50%, 0%, 0)`,
                zIndex: 999999999,
            }}
        >
            <Input onChange={handleChange} value={String(spiral.size)} />
            <Button onClick={handleDecrease} disabled={spiral.size <= 0}>
                -
            </Button>
            <Button onClick={handleIncrease}>+</Button>
        </div>
    );
};

export default styled(observer(Config))({});
