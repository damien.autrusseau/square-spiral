import { FC } from "react";
import { styled } from "@mui/material/styles";
import { keyframes } from "@emotion/react";
import spiral, { Chunk } from "../states/spiral";

const SpiralBox: FC<Chunk> = (props): JSX.Element => {
    const { index, x, y, vx, vy, scale } = props;
    return (
        <Box
            {...props}
            style={{
                top: `${y * 100}%`,
                left: `${x * 100}%`,
                width: `${scale * 1.5 * 100}%`,
                height: `${scale * 1.5 * 100}%`,
                zIndex: index,
                transitionDelay: `${index * 0.005}s`,
                animationDelay: `${index * 0.0005}s`,
            }}
        >
            <Shape
                {...props}
                style={{
                    transform: `rotate(${
                        (Math.atan2(vx, vy) * 180) / Math.PI
                    }deg) `,
                    // transformOrigin: "left top ",
                    background: `hsl(${index * 5},50%,70%)`,
                    fontSize: `${20 - 0.5 * spiral.size}px`,
                }}
            />
            <Text {...props} style={{}}>
                {index}
            </Text>
        </Box>
    );
};

const easeIn = keyframes`
  0% {
    top: 50%;
    left: 50%;
    opacity: 0;
  }
  100% {
    opacity: 1;
  }
`;

const Box = styled("div")({
    maxWidth: 200,
    maxHeight: 200,
    opacity: 1,
    transition: "all ease-out 0.4s ",
    position: "absolute",
    transform: `translate3d(-50%, -50%, 0)`,
    transformOrigin: "left top ",
    animation: `${easeIn} 0.4s ease`,
    "&:hover": {
        transition: "",
        animation: "",
        transitionDelay: 0,
        animationDelay: 0,
        transform: `translate3d(-50%, -50%, 0) scale(1.2)`,
        transformOrigin: "center center ",
        zIndex: "99999999 !important",
    },
});

const Shape = styled("div")({
    position: "absolute",
    top: `0`,
    left: `0`,
    right: 0,
    bottom: 0,
    zIndex: 0,
    boxShadow: "#565656 0px 10px 18px -3px",
});

const Text = styled("div")({
    position: "absolute",
    top: `50%`,
    left: `50%`,
    zIndex: 1,
    transform: `translate3d(-50%, -50%, 0)`,
});

export default SpiralBox;
