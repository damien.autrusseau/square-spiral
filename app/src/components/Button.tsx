import { MouseEventHandler } from "react";
import { styled } from "@mui/material/styles";
import { FC, ReactNode } from "react";

interface Props {
    onClick: MouseEventHandler<HTMLButtonElement>;
    children: ReactNode;
    disabled?: boolean;
}

const Button: FC<Props> = ({ onClick, children, disabled }) => {
    return (
        <button disabled={disabled} onClick={onClick}>
            {children}
        </button>
    );
};

const SytledButton = styled(Button)({});
export default SytledButton;
