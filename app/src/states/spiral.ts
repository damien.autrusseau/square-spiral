import { makeAutoObservable } from "mobx";

export type Chunk = {
    index: number;
    scale: number;
    x: number;
    y: number;
    vx: number;
    vy: number;
};

const spiral = new (class Spiral {
    size: number = 6;
    count: number = this.size * this.size;
    width: number = 100;

    constructor() {
        makeAutoObservable(this);
    }

    public increaseSize(): void {
        this.setSize(this.size + 1);
    }

    public decreaseSize(): void {
        this.setSize(this.size - 1);
    }

    // Function overloading example ("like")
    public setSize(value: string): void;
    public setSize(value: number): void;
    public setSize(value: any): void {
        let size = value;
        if (typeof value === "string") {
            size = parseInt(size);
        }
        this.size = Math.max(0, Math.min(30, size));
        this.count = this.size * this.size;
    }

    public getChunks(): Chunk[] {
        const chunks: Chunk[] = [];
        let index: number = 1;
        let x0: number = 0;
        let xn: number = this.size - 1;
        let y0: number = 0;
        let yn: number = this.size - 1;
        const n: number = this.size - 1;

        const add = (x: number, y: number, vx: number, vy: number) => {
            chunks.push({
                index,
                scale: 1 / (spiral.size + 1),
                y: y / n,
                x: x / n,
                vx,
                vy,
            });
            index++;
        };

        while (x0 <= xn && y0 <= yn) {
            for (let x = x0; x <= xn; x++) {
                add(x, y0, 1, 0);
            }
            y0++;
            for (let y = y0; y <= yn; y++) {
                add(xn, y, 0, -1);
            }
            xn--;
            for (let x = xn; x >= x0; x--) {
                add(x, yn, -1, 0);
            }
            yn--;
            for (let y = yn; y >= y0; y--) {
                add(x0, y, 0, 1);
            }
            x0++;
        }
        return chunks;
    }
})();

export default spiral;
