import Config from "./components/Config";
import Spiral from "./components/Spiral";
import CssBaseline from "@mui/material/CssBaseline";

const App = (): JSX.Element => {
    return (
        <>
            <CssBaseline />
            <div>
                <Config />
                <Spiral />
            </div>
        </>
    );
};

export default App;
