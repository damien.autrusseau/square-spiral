#!/bin/bash

PROJECT="square-spiral"

PORT=3000

ENVS="\
    --env PROJECT=$PROJECT \
"

dev() {
    echo -e "[$PROJECT] Starting dev..."
    docker build  -t ${PROJECT} ./ 
    docker run -tti $ENVS --rm --name ${PROJECT} -v $(pwd)/app/:/app/ -p "$PORT:3000" ${PROJECT}
}

yarn() {
    docker run -tti $ENVS --rm --name ${PROJECT} -v $(pwd)/app/:/app/ -p "$PORT:3000" ${PROJECT} yarn $@
}
$@