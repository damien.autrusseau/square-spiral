# Description
Square Spiral is a project as "a proof of skills" (react, maths)

# requirements
- docker [https://docs.docker.com/engine/install/] version >= 20.10.9
- bash (Script Shell Interpreter) version >= 5.0.17

# Rules
Generate an HTLM Dom square spiral with edge sizes configurable from input and buttons 

# Getting started 
```bash
git clone https://gitlab.com/damien.autrusseau/square-spiral.git square-spiral
cd square-spiral/
./do dev
```

Open a browser at http://127.0.0.1:3000/ 

# In DEV commands
```bash
./do build
./do yarn add [...]
./do tests
```
