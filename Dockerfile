FROM node:14-buster AS builder

ENV NODE_OPTIONS='--max_old_space_size=4096'
ENV NEXT_TELEMETRY_DISABLED=1
ENV TSC_COMPILE_ON_ERROR=true
ENV CHOKIDAR_USEPOLLING=true

RUN apt update
RUN apt install -y gettext

WORKDIR /tmp/
COPY package.json yarn* ./
RUN yarn --network-timeout 100000

WORKDIR /app/
COPY ./ ./
RUN yarn --network-timeout 100000

ENTRYPOINT ["/bin/bash", "/app/entrypoint"]
CMD ["start"]